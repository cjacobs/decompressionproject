#include "LZWExp.h"
#include "SmartAlloc.h"
#include <stdio.h>
#include <stdlib.h>

void TestExpOutput(void *state, char *data, int numBytes) {
    
   if (data) {
      while (numBytes) {
         printf(state, *data++);
         numBytes--;
      }
   }
}

int main() {
   LZWExp *myExp;
   int scanResult = 0;
   int decodedValue = 0;
   int leftVal = 0;
   int wasStopped = 0;
   UInt input = 0;
   FILE *fp;

   fp = fopen("file.txt", "w");
   fprintf(fp, "%X", input);
   fclose(fp);
  
   do {  

   myExp = malloc(sizeof(LZWExp));
   LZWExpInit(myExp, TestExpOutput, "%c");
   
   fp = fopen("file.txt", "r");
   while (scanResult != EOF && decodedValue == 0) {
      scanResult = fscanf(fp, "%X", &input);
        //printf("this works\n");
      if (scanResult - EOF) {
            //printf("this works\n")

         decodedValue = LZWExpDecode(myExp, input);
      }
   //printf("curData = %X, nextData = %X, bitsLeft= %d, validNext = %d\n", (myExp->bitUnpacker).curData, (myExp->bitUnpacker).nextData, (myExp->bitUnpacker).bitsLeft, (myExp->bitUnpacker).validNext);
   }
   printf("input = %d read\n", input);
   fclose(fp);
    //printf("%d\n", EOF);
   wasStopped = LZWExpStop(myExp);
   if (decodedValue == BAD_CODE || leftVal) {
      printf("Bad code\n");
   }
   else if (wasStopped) {
      printf("Missing EOD\n");
   }
    
   LZWExpDestruct(myExp);
   free(myExp);
   
   if (report_space()) {
      printf("space: %d\n", report_space());
   }
   input++;
   fp = fopen("file.txt", "w");
   fprintf(fp, "%X", input);
   fclose(fp);
   } while(input > 0);   
}
