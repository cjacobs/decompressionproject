#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "SmartAlloc.h"
#include "CodeSet.h"

#include <stdio.h>

typedef struct CodeEntry2 {
    char code;
    int size;
    Code *getCode;
    int wasFreed;
    struct CodeEntry2 *prefixCode;
} CodeEntry2;

typedef struct CodeSet {
    CodeEntry2 *codes;
    int codeCount;
} CodeSet;


/* Allocate, initialize, and return a CodeSet object, via void *
 * The CodeSet will have room for |numCodes| codes, though it will
 * initially be empty. */
void *CreateCodeSet(int numCodes) {
    CodeSet *set = malloc(sizeof(CodeSet));
    set->codeCount = numCodes;
    set->codes = calloc(numCodes, sizeof(CodeEntry2));
    CodeEntry2 *iterateCode = set->codes;
    int i;
    for(i=0; i<numCodes; i++) {
        //(iterateCode+i)->code = malloc(sizeof(Code));
        //(iterateCode+i)->prefixCode= NULL;
        //(iterateCode+i)->code = malloc(sizeof(char));
        (iterateCode+i)->size = 0;
    }
    return set;
}

/* Add a new 1-byte code to |codeSet|, returning its index, with
 * the first added code having index 0.  The new code's byte is
 * equal to |val|.  Assume (and assert if needed) that there
 * is room in the |codeSet| for a new code. */
int NewCode(void *codeSet, char val) {
    CodeSet *set = codeSet;
    CodeEntry2 *testCode = set->codes;
    int i;
    for(i=0; i<set->codeCount && (testCode+i)->size != 0; i++) {
    }
    testCode = testCode+i;
    //testCode->code = malloc(sizeof(char));
    //testCode->code->data = malloc(sizeof(char));
    testCode->code = val;
    testCode->size = 1;
    return i;
}

/* Create a new code by copying the existing code at index
 * |oldCode| and extending it by one zero-valued byte.  Any
 * existing code might be extended, not just the most recently
 * added one. Return the new code's index.  Assume |oldCode|
 * is a valid index and that there is enough room for a new code. */
int ExtendCode(void *codeSet, int oldCode) {
    CodeSet *set = codeSet;
    CodeEntry2 *testCode = set->codes;
    CodeEntry2 *newCode = set->codes+oldCode;
    int i;
    /*char *newSequence = calloc(newCode->code->size +1, sizeof(char));
     
     for(i=0; i<newCode->code->size; i++) {
     *(newSequence+i) = *(newCode->code->data+i);
     }
     *(newSequence+i) = (char)0;*/
    for(i=oldCode; i<set->codeCount && (testCode+i)->size != 0; i++) {
    }
    testCode = testCode+i;
    testCode->prefixCode = newCode;
    //testCode->code = malloc(sizeof(char));
    testCode->code = (char)0;
    testCode->size = newCode->size+1;
    return i;
}

/* Set the final byte of the code at index |code| to |suffix|.
 * This is used to override the zero-byte added by ExtendCode.
 * If the code in question has been returned by a GetCode call,
 * and not yet freed via FreeCode, then the changed final byte
 * will also show in the Code data that was returned from GetCode.*/
void SetSuffix(void *codeSet, int code, char suffix) {
    CodeSet *set = codeSet;
    CodeEntry2 *theCodeEntry = set->codes+code;
    //Code *theCode = theCodeEntry->code;
    //char *finalByte = theCodeEntry->code;->data;
    theCodeEntry->code = suffix;
    
}

/* Return the code at index |code| */
Code GetCode(void *codeSet, int code) {
    CodeSet *set = codeSet;
    CodeEntry2 *theCodeEntry = set->codes + code;
    CodeEntry2 *temp = theCodeEntry;
    
    int tempsize = theCodeEntry->size;
    theCodeEntry->getCode = malloc(sizeof(Code));
    theCodeEntry->getCode->data = malloc(theCodeEntry->size * sizeof(char));
    theCodeEntry->getCode->size = tempsize;
    char *textData = theCodeEntry->getCode->data;
    /*while(temp->prefixCode) {
     tempsize--;
     while (tempsize-temp->prefixCode->size) {
     *(textData+tempsize) = *(temp->code +tempsize-temp->prefixCode->size);
     tempsize--;
     }
     *(textData+tempsize) = *(temp->code +tempsize-temp->prefixCode->size);
     temp = temp->prefixCode;
     }
     while(tempsize--){
     *(textData+tempsize) = *(temp->code +tempsize);
     }*/
    while (temp->prefixCode) {
        tempsize--;
        *(textData+tempsize) = temp->code;
        temp = temp->prefixCode;
    }
    while(tempsize--){
        *(textData+tempsize) = temp->code;
    }
    
    theCodeEntry->wasFreed = 0;
    int i;
    for(i= 0; i < theCodeEntry->size; i++) {
        printf(" %d", *(textData+i));
    }
    printf("\n");
    return *(theCodeEntry->getCode);
    
}

/* Mark the code at index |code| as no longer needed, until a new
 * GetCode call is made for that code. */
void FreeCode(void *codeSet, int code) {
    CodeSet *set = codeSet;
    CodeEntry2 *theCodeEntry = set->codes + code;
    Code *theCode = theCodeEntry->getCode;
    
    //free(theCodeEntry->resultCode->data);
    //free(theCodeEntry->resultCode);
    //
    if (theCodeEntry->wasFreed == 0) {
        free(theCode->data);
        free(theCode);
        theCodeEntry->wasFreed = 1;
    }
    
    //free(theCodeEntry);
}

/* Free all dynamic storage associated with |codeSet| */
void DestroyCodeSet(void *codeSet) {
    CodeSet *set = codeSet;
    CodeEntry2 *testCode = set->codes;
    /*int i;
     for (i=0; i<set->codeCount && (testCode+i)->size != 0; i++) {
     //free(testCode+i)
     free((testCode+i)->code);
     //free((testCode+i));
     }*/
    free(testCode);
    free(set);
}
