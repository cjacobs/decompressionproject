#include <stdio.h>
#include "LZWExp.h"
#include "CodeSet.h"
#include "MyLib.h"
#include "SmartAlloc.h"


void LZWExpFill(LZWExp *exp, int mode) {
   int numCodes;
    
   if (mode) {
      DestroyCodeSet(exp->dict);
   }
   exp->dict = CreateCodeSet(RECYCLE_CODE);
   for (numCodes = 0; numCodes <= CHAR_SIZE; numCodes++) {
      NewCode(exp->dict, numCodes);
   }
   exp->lastCode = CHAR_SIZE;
   exp->numBits = CODE_LENGTH;
   exp->maxCode = (1 << exp->numBits) - 1;
}


int LZWExpFindCode(LZWExp *exp, UInt code) {

   if (exp->EODSeen) {
      return code ? BAD_CODE : 0;
   }
   if (exp->lastCode < code) {
      return BAD_CODE;
   }
   if (code != CHAR_SIZE) {
      Code aCode = GetCode(exp->dict, code);
      
      SetSuffix(exp->dict, exp->lastCode, *aCode.data);      
      exp->sink(exp->sinkState, (char *)aCode.data, aCode.size);
      if (exp->lastCode < RECYCLE_CODE - 1) {
         ExtendCode(exp->dict, code);
      }
      exp->lastCode++;
      FreeCode(exp->dict, code);
   }
   else {
      exp->EODSeen = 1;
   }

   return 0;
}


void LZWExpInit(LZWExp *exp, DataSink sink, void *sinkState) {
   exp->sink = sink;
   exp->sinkState = sinkState;
   LZWExpFill(exp, 0);
   buInit(&(exp->bitUnpacker));
   exp->EODSeen = 0;
}

/* Break apart compressed data in "bits" into one or more codes and send
 * the corresponding symbol sequences to the DataSink.  Save any leftover
 * compressed bits to combine with the bits from the next call of
 * LZWExpEncode.  Return 0 on success or BAD_CODE if you receive a code not
 * in the dictionary.
 *
 * For this and all other methods, a code is "invalid" if it either could not
 * have been sent (e.g. is too high) or if it is a nonzero code following
 * the detection of an EOD code.
 */

int LZWExpDecode(LZWExp *exp, UInt bits) {
   int bitsToGo = INT_SIZE;
   UInt value;
   static UInt partialValue = 0;
   static int tempsize = 0;
   int temp;

   buTakeData(&(exp->bitUnpacker), bits);

   if (exp->EODSeen) {
      return BAD_CODE;
   }
   while (bitsToGo >= exp->numBits) {
      temp = bits & exp->maxCode >> (exp->numBits - tempsize);

      if (exp->maxCode < exp->lastCode) {
         exp->maxCode = (1 << ++(exp->numBits)) - 1;
      }  
      if (tempsize != 0) {
         unpack(&(exp->bitUnpacker), tempsize, &value);
         bitsToGo -= tempsize;
         tempsize = 0;

         value = partialValue | value;
         partialValue = 0;
      }
      else {
         bitsToGo -= exp->numBits;
         unpack(&(exp->bitUnpacker), exp->numBits, &value);
      }  
      if (LZWExpFindCode(exp, value)) {
         return BAD_CODE;
      }
      if (exp->lastCode == RECYCLE_CODE) {
         LZWExpFill(exp, 1);
      }
   }
   if (bitsToGo > 0) {
      tempsize = exp->numBits - bitsToGo;
      unpack(&(exp->bitUnpacker), bitsToGo, &partialValue);
      
      if (exp->EODSeen && partialValue > 0) {
         return BAD_CODE;
      }
      partialValue <<= tempsize;
   }
    
   temp = bits & exp->maxCode >> (exp->numBits - tempsize);

   return 0;    
}


int LZWExpStop(LZWExp *exp) {
    
   if (exp->EODSeen) {
      exp->EODSeen = 0;
      return 0;
   }
   else {
      return NO_EOD_CODE;
   }
}

void LZWExpDestruct(LZWExp *exp) {
   DestroyCodeSet(exp->dict);
}
