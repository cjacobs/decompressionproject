#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UINT_SIZE 32
#define UINT_MASK 0xFFFFFFFF

typedef unsigned int UInt;

typedef struct {
   UInt curData;
   UInt nextData;
   int bitsLeft;
   int validNext;
} BitUnpacker;

void buInit(BitUnpacker *bu) {
   bu->curData = 0;
   bu->nextData = 0;
   bu->bitsLeft = UINT_SIZE;
   bu->validNext = 1;
}

void buTakeData(BitUnpacker *bu, UInt val) {
   bu->nextData = val;
}

int unpack(BitUnpacker *bu, int size, UInt *result) {
   if (bu->validNext) {
      
      bu->validNext = 0;

      if (bu->bitsLeft < 0 && bu->bitsLeft+UINT_SIZE) {
            bu->curData <<= -bu->bitsLeft;
            bu->curData |= bu->nextData>>(UINT_SIZE+bu->bitsLeft);
            *result = bu->curData & UINT_MASK;
            bu->nextData = bu->nextData<<-bu->bitsLeft;
            bu->bitsLeft +=UINT_SIZE;
      }
      else {
         bu->bitsLeft = UINT_SIZE;
      }
      
      return 0;
   }
      
      if (size > bu->bitsLeft && bu->bitsLeft > 0) {
           bu->curData = bu->nextData>>(UINT_SIZE-bu->bitsLeft);
           bu->nextData = bu->nextData<<bu->bitsLeft;
         }
         else {
           bu->curData = bu->nextData>>(UINT_SIZE-size);
           bu->nextData = bu->nextData<<size;
            
        }
      *result = bu->curData & UINT_MASK;
      bu->bitsLeft -= size; 
     
      if (!bu->nextData || bu->bitsLeft == -UINT_SIZE) {
         bu->validNext = 1;
      }
      return bu->bitsLeft>0?1:0;
      
}
