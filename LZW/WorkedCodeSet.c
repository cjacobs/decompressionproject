/*Silver*/

#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "SmartAlloc.h"
#include "CodeSet.h"

#include <stdio.h>

//#define MAXSIZE 15
:
typedef struct CodeEntry2 {
   char fullCode[MAXSIZE];
   int size;
   //int numCalls;
   //char code;
   //int numIncludes;
   struct CodeEntry2 *prefixCode;
} CodeEntry2;

typedef struct CodeSet {
   CodeEntry2 *codes;
   short codeCount;
   char *getCode;
   int numCalls;
   //int sameValue;
   //int wasFreed;
} CodeSet;


/* Allocate, initialize, and return a CodeSet object, via void *
 * The CodeSet will have room for |numCodes| codes, though it will
 * initially be empty. */
void *CreateCodeSet(int numCodes) {
   CodeSet *set = malloc(sizeof(CodeSet));

   set->codeCount = numCodes;
   set->codes = calloc(numCodes, sizeof(CodeEntry2));
   //set->getCode = calloc(numCodes, sizeof(char));
   set->codeCount = 0;
   set->numCalls = 0;

   return set;
}

/* Add a new 1-byte code to |codeSet|, returning its index, with
 * the first added code having index 0.  The new code's byte is
 * equal to |val|.  Assume (and assert if needed) that there
 * is room in the |codeSet| for a new code. */
int NewCode(void *codeSet, char val) {
   CodeSet *set = codeSet;
   int index;
   CodeEntry2 *testCode;

   testCode = set->codes + set->codeCount;
   //testCode->fullCode = malloc(sizeof(char)); 
   //testCode = set->codes + set->codeCount;
   //testCode->numCalls = 0;
   *(testCode->fullCode) = val;
   //printf("%c\n", testCode->fullCode);
   testCode->size = 1; 
   index = set->codeCount;
   set->codeCount += 1;

   return index + 1;
}

/* Create a new code by copying the existing code at index
 * |oldCode| and extending it by one zero-valued byte.  Any
 * existing code might be extended, not just the most recently
 * added one. Return the new code's index.  Assume |oldCode|
 * is a valid index and that there is enough room for a new code. */
int ExtendCode(void *codeSet, int oldCode) {
   CodeSet *set = codeSet;
   int index;
   //int codeNum = 0;
   //CodeEntry2 *theOldCode = set->codes + oldCode;
   CodeEntry2 *testCode = set->codes + set->codeCount;
   CodeEntry2 *aCode = testCode->prefixCode;
   //testCode->fullCode = NULL;

   testCode->prefixCode = set->codes + oldCode;
   testCode->size = testCode->prefixCode->size + 1;
   //if (set->codeCount != oldCode) {
     // testCode =calloc(1, sizeof(char));
     // *(testCode->fullCode) = 0;
   //}
   //else {
      //testCode->size += 1;
   //testCode->fullCode = calloc(testCode->size, sizeof(char));
      //*(testCode->fullCode) = *(testCode->prefixCode->fullCode);
      //testCode->numIncludes =2;
      //testCode->fullCode = 0;
   //}

   for (index = 0; index < aCode->size; index++) {
      *(testCode->fullCode + index) = *(aCode->fullCode + index);
   }
   //*(testCode->fullCode+testCode->size-1) = 0;
  // testCode->numCalls = 0;

   index = set->codeCount;
   set->codeCount += 1;

   return index + 1;
}

/* Set the final byte of the code at index |code| to |suffix|.
 * This is used to override the zero-byte added by ExtendCode.
 * If the code in question has been returned by a GetCode call,
 * and not yet freed via FreeCode, then the changed final byte
 * will also show in the Code data that was returned from GetCode.*/
void SetSuffix(void *codeSet, int code, char suffix) {
   CodeSet *set = codeSet;
   CodeEntry2 *curCode = set->codes + code;   

   //int diff = 0;
   /*if (curCode->prefixCode) {
      diff = curCode->size-curCode->prefixCode->size -1;
   }
   if (diff < 0) {
      diff = 0;
   }*/

  //if (suffix != (char)0) { 
  //if (set->codeCount != code) {
   *curCode->fullCode = suffix;
  /*}
  else {
     * (curCode->fullCode+ 1) = *(curCode->fullCode);
  }*/
   //curCode->fullCode = suffix;
  // }
  /* else {
      curCode = set->codes + code;
      if (curCode->prefixCode)  {
         while (curCode->prefixCode) {
            curCode = curCode->prefixCode;
         } 
         //printf("\nmycode is %c\n", (set->codes + code)->prefixCode->code);
         (set->codes + code)->code = curCode->code;
      }
      else {
         (set->codes + code)->code = suffix;
      }
   }*/
   //(set->codes + code)->numCalls = 0; 
}

/* Return the code at index |code| */
Code GetCode(void *codeSet, int code) {
   CodeSet *set = codeSet;
   CodeEntry2 *temp = set->codes + code;
   CodeEntry2 *curCode = temp;
   //char *textData;    
   Code resultCode;
   int tempsize = 0;
   int theSize;  

   while (temp) {
      tempsize++;
      temp = temp->prefixCode;
   }
   theSize = tempsize;

   if (set->numCalls == 0) {
      set->getCode = calloc(tempsize, sizeof(char));
   }
   set->numCalls += 1;
   temp = curCode;

   //textData = temp->fullCode;
   //temp = temp->prefixCode;
   while (tempsize--) {
      /*if( temp->fullCode != 0) {
         *(set->getCode + tempsize) = temp->prefixCode->fullCode;
      }
      else {*/
         //printf("%c\n", temp->fullCode);)
      /*while(tempsize - temp->size > 0) {
         *(set->getCode + tempsize) = *(temp->fullCode+ (tempsize- temp->size));
         tempsize--;
      }*/

      *(set->getCode + tempsize) = *temp->fullCode;
      //if (code == set->codeCount) {
         //printf("in here");
         //tempsize--;
         //*(set->getCode + tempsize) = temp->fullCode;
      //}
      temp = temp->prefixCode;    
   }
   //while (tempsize--) {
     // if (temp->code > 0) {
   //*(set-> + tempsize--) = temp->code;
     //}
   //}
   //temp = set->codes + code;
   //temp->wasFreed = 1;
   resultCode.data = set->getCode;
   resultCode.size = theSize;
   return resultCode;
    
}

/* Mark the code at index |code| as no longer needed, until a new
 * GetCode call is made for that code. */
void FreeCode(void *codeSet, int code) {
   CodeSet *set = codeSet;
   CodeEntry2 *theCodeEntry = set->codes + code;

   set->numCalls -= 1;
   if (set->numCalls == 0) {
      //theCodeEntry->wasFreed = 0;
      //free(theCodeEntry->fullCode);
      free(set->getCode);
      set->getCode = NULL;
      //theCodeEntry->fullCode = NULL;
   }
    
}

/* Free all dynamic storage associated with |codeSet| */
void DestroyCodeSet(void *codeSet) {
   CodeSet *set = codeSet;
   CodeEntry2 *testCode = set->codes;
   CodeEntry2 *temp;

   /*int i = 0;
   while (i<set->codeCount) {
      temp = set->codes + i;
      if (temp->fullCode) {
       // printf("%p %p\n", temp->fullCode, NULL);
         free(temp->fullCode);
        // printf("did this");
      }
      i++;
      //i++;
   }*/
   /*if (set->getCode) {
      free(set->getCode);
   }*/
   free(set->codes);
   free(set);
}
