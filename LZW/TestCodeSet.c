
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "SmartAlloc.h"
#include "CodeSet.h"

#include <stdio.h>



typedef struct CodeEntry {
   char allocated;
   char data;
   struct CodeEntry *prefix;
   Code codeAtIndex;
} CodeEntry;

typedef struct CodeSet {
        CodeEntry *set;
        int size;
        int end;
} CodeSet;
        
/* Allocate, initialize, and return a CodeSet object, via void *
 *  *  * The CodeSet will have room for |numCodes| codes, though it will
 *   *   * initially be empty. */
void *CreateCodeSet(int numCodes){
        CodeSet *cset = calloc(1, sizeof(CodeSet));
        cset->set = calloc(numCodes, sizeof(CodeEntry));
        CodeEntry *currentSet = cset->set;
        cset->size = numCodes;
        return cset;
}
        
        
/* Add a new 1-byte code to |codeSet|, returning its index, with
 *  *  * the first added code having index 0.  The new code's byte is
 *   *   * equal to |val|.  Assume (and assert, in order to pass style)
 *    *    * that there is room in the |codeSet| for a new code. */
int NewCode(void *codeSet, char val){
        CodeSet *cset = codeSet;
        CodeEntry *newEntry = cset->set + cset->end;
        newEntry->data = val;
        return cset->end++;
}
        
        
/* Create a new code by extending the existing code at index
 *  *  * by one zero-valued byte, using the linked structure from spec.  Any
 *   *   * existing code might be extended, not just the most recently
 *    *    * added one. Return the new code's index.  Assume |oldCode| 
 *     *     * is a valid index, and that there is enough room for 
 *      *      * a new code, but assert both of these conditions as well */
int ExtendCode(void *codeSet, int oldCode){
        CodeSet *cset = codeSet;
        CodeEntry *newEntry = cset->set + cset->end;
        newEntry->prefix = cset->set + oldCode;
        return cset->end++;
}       
        
/* Set the final byte of the code at index |code| to |suffix|.  
 *  *  * This is used to override the zero-byte added by extendCode. 
 *   *   * If the code in question has been returned by a getCode call,
 *    *    * and not yet freed via freeCode, then the changed final byte
 *     *     * will also show in the Code data that was returned from getCode.*/
void SetSuffix(void *codeSet, int code, char suffix){
        CodeSet *cset = codeSet;
        CodeEntry *current = cset->set + code;
        current->data = suffix;
        if(current->allocated){
                *(current->codeAtIndex.data) = suffix;
        }

}

/* Return the code at index |code| */
Code GetCode(void *codeSet, int code){ 
        int size = 0;
        CodeSet *cset = codeSet;
        CodeEntry *current = cset->set + code;
        Code *newCode = &current->codeAtIndex;
        if(current->allocated++ == 0){
                assert(newCode);
                CodeEntry *temp = current;
                for(;temp; size++, temp = temp->prefix);
                unsigned char *string = calloc(size, sizeof(char));
                newCode->size = size;
                newCode->data = string;
		string += size-1;
                for(;current; *string-- = current->data, current = current->prefix) {
			if(current->data == '\0') {
				string++;
			}
			//current = current->prefix;
		}
        }
        return *newCode;
}

/* Mark the code at index |code| as no longer needed, until a new
 *  *  * getCode call is made for that code. */
void FreeCode(void *codeSet, int code){
        CodeSet *cset = codeSet;
        CodeEntry *current = cset->set + code;
        if(current->allocated){
                if(!--current->allocated){
                        free(current->codeAtIndex.data);
                }
        }
}

/* Free all dynamic storage associated with |codeSet| */
void DestroyCodeSet(void *codeSet){
        CodeSet *cset = codeSet;
        int numCode = cset->end;
        CodeEntry *current = cset->set;
        while(numCode){
                FreeCode(codeSet, --numCode);
        }
        free(cset->set);
        free(cset);
}
