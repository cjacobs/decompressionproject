/*Silver*/

#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "SmartAlloc.h"
#include "CodeSet.h"

#include <stdio.h>


typedef struct CodeEntry2 {
   char code;
   int size;
   int numCalls;
   //int wasFreed;
   struct CodeEntry2 *prefixCode;
} CodeEntry2;

typedef struct CodeSet {
   CodeEntry2 *codes;
   int codeCount;
   Code *getCode;
   int wasFreed;
} CodeSet;


/* Allocate, initialize, and return a CodeSet object, via void *
 * The CodeSet will have room for |numCodes| codes, though it will
 * initially be empty. */
void *CreateCodeSet(int numCodes) {
   CodeSet *set = malloc(sizeof(CodeSet));

   set->codeCount = numCodes;
   set->codes = calloc(numCodes, sizeof(CodeEntry2));
   set->getCode = calloc(numCodes, sizeof(Code));
   set->codeCount = 0;

   return set;
}

/* Add a new 1-byte code to |codeSet|, returning its index, with
 * the first added code having index 0.  The new code's byte is
 * equal to |val|.  Assume (and assert if needed) that there
 * is room in the |codeSet| for a new code. */
int NewCode(void *codeSet, char val) {
   CodeSet *set = codeSet;
   int index;
   CodeEntry2 *testCode;

   testCode = set->codes + set->codeCount;
   testCode->numCalls = 0;
   testCode->code = val;
   testCode->size = 1; 
   index = set->codeCount;
   set->codeCount += 1;

   return index;
}

/* Create a new code by copying the existing code at index
 * |oldCode| and extending it by one zero-valued byte.  Any
 * existing code might be extended, not just the most recently
 * added one. Return the new code's index.  Assume |oldCode|
 * is a valid index and that there is enough room for a new code. */
int ExtendCode(void *codeSet, int oldCode) {
   CodeSet *set = codeSet;
   int index;
   CodeEntry2 *testCode;
 
   testCode = set->codes + set->codeCount;
   testCode->prefixCode = set->codes + oldCode;
   testCode->code = (char)0;
   testCode->size = testCode->prefixCode->size + 1;
   testCode->numCalls = 0;

   index = set->codeCount;
   set->codeCount += 1;
   //printf("done with extending\n");

   return index;
}

/* Set the final byte of the code at index |code| to |suffix|.
 * This is used to override the zero-byte added by ExtendCode.
 * If the code in question has been returned by a GetCode call,
 * and not yet freed via FreeCode, then the changed final byte
 * will also show in the Code data that was returned from GetCode.*/
void SetSuffix(void *codeSet, int code, char suffix) {
   CodeSet *set = codeSet;

   //if (code < set->codeCount) {
   (set->codes + code)->code = suffix;
   (set->codes + code)->numCalls = 0; 
   //}
   //printf("done with Set suffix\n");
}

/* Return the code at index |code| */
Code GetCode(void *codeSet, int code) {
   CodeSet *set = codeSet;
   CodeEntry2 *temp = set->codes + code;
   char *textData;    
   Code resultCode = *(set->getCode +code);
   int tempsize = temp->size;  
   //printf("tempsize = %d\n", tempsize);
   /*if (temp->numCalls > 0) {
      resultCode = *(set->getCode+code);	

   }*/
   if (temp->numCalls == 0) {
      /*if (resultCode.data != NULL) {
         free(resultCode.data);
      }*/
      //set->getCode+code = calloc(temp->size, sizeof(Code));
   
   //temp->numCalls += 1;
   //printf("not there\n");
   textData = calloc(tempsize, sizeof(char));
   //printf("got here\n");
   resultCode.data = textData;
   textData += tempsize-1;
   for (;temp; *textData-- = temp->code, temp=temp->prefixCode) {
      tempsize--;
      if() {

      }
   }
   /*while (temp->prefixCode) {
      //if (temp->code > 0) {
         *(textData + (--tempsize)) = temp->code;
      //}
      temp = temp->prefixCode;    
   }
   while (tempsize) {
     // if (temp->code > 0) {
     *(textData + (--tempsize)) = temp->code;
     //}
   }*/
   //printf("got here\n");
   //temp->wasFreed = 1;
   //resultCode.data = textData;
   resultCode.size = (set->codes + code)->size;
   }
   temp = set->codes + code;
   temp->numCalls += 1;
   //printf("got here");
   return resultCode;
    
}

/* Mark the code at index |code| as no longer needed, until a new
 * GetCode call is made for that code. */
void FreeCode(void *codeSet, int code) {
   CodeSet *set = codeSet;
   CodeEntry2 *theCodeEntry = set->codes + code;
   Code aCode = *(set->getCode + code);

   //printf("not there\n");

   theCodeEntry->numCalls -= 1;
   if (theCodeEntry->numCalls == 0) {
      //theCodeEntry->wasFreed = 0;
      if (aCode.data) {
         free(aCode.data);
         //aCode.data = NULL;
      }
      //free(aCode);
      //set->getCode == NULL;
   }
   //printf("got here\n");
}

/* Free all dynamic storage associated with |codeSet| */
void DestroyCodeSet(void *codeSet) {
   CodeSet *set = codeSet;
   CodeEntry2 *testCode = set->codes;
   //int codesLeft= set->codeCount-1;
   //Code *curCode; 

   //while (codesLeft) {
      //curCode = set->getCode + c
      //FreeCode(codeSet, codesLeft--);
   //} 
   /*if (set->getCode) {
      free(set->getCode);
   }*/
   //printf("IN HERE\n");
   free(set->getCode);
   free(set->codes);
   free(set);

   /*if(report_space()) {
     printf("\nspace: %d\n", report_space);
   }*/
}
