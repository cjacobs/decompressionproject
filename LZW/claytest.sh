#!/bin/sh
echo "testing bronze 1"
./a.out < tests/tests/Bronze/test1.in > mytest.out
diff tests/tests/Bronze/test1.out mytest.out
echo "testing bronze 2"
./a.out < tests/tests/Bronze/test2.in > mytest.out
diff tests/tests/Bronze/test2.out mytest.out
echo "testing bronze 3"
./a.out < tests/tests/Bronze/test3.in > mytest.out
diff tests/tests/Bronze/test3.out mytest.out
echo "testing bronze 4"
./a.out < tests/tests/Bronze/test4.in > mytest.out
diff tests/tests/Bronze/test4.out mytest.out
echo "testing bronze 9"
./a.out < tests/tests/Silver/bronzetest9.in > mytest.out
diff tests/tests/Silver/bronzetest9.out mytest.out
echo "testing bronze 12"
./a.out < tests/tests/Silver/bronzetest12.in > mytest.out
diff tests/tests/Silver/bronzetest12.out mytest.out
echo "testing bronze 13"
./a.out < tests/tests/Silver/bronzetest13.in > mytest.out
diff tests/tests/Silver/bronzetest13.out mytest.out
echo "testing Silver 1"
./a.out < tests/tests/Silver/test1.in > mytest.out
diff tests/tests/Silver/test1.out mytest.out
echo "testing Clay's test 1"
./a.out < test.txt.Z > mytest.out
./Ref < test.txt.Z > reftest.out
diff reftest.out mytest.out
echo "testing Clay's test 2"
./a.out < file.txt.Z > mytest.out
./Ref < file.txt.Z > reftest.out
diff reftest.out mytest.out
echo "testing Clay's test 3"
./a.out < LZWProject.pdf.Z > mytest.out
./Ref < LZWProject.pdf.Z > reftest.out
diff reftest.out mytest.out
echo "testing Clay's test 4"
./a.out < spectrum.jpg.Z > mytest.out
./Ref < spectrum.jpg.Z > reftest.out
diff reftest.out mytest.out
