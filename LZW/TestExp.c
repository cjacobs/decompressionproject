#include "LZWExp.h"
#include "SmartAlloc.h"
#include <stdio.h>
#include <stdlib.h>

void TestExpOutput(void *state, char *data, int numBytes) {
   int origByte = numBytes;
   int shouldPrint = 1;
   int numVals = 0; 
   char *tempData = data;


   if (data) {
      while (numBytes) {
         printf(state, *data++);
         numBytes--; 
      }
   }
}

int main() {
   LZWExp *myExp = malloc(sizeof(LZWExp));
   int scanResult = 0;
   int decodedValue = 0;
   int wasStopped = 0;
   UInt input;
    
   LZWExpInit(myExp, TestExpOutput, "%c");    
   while (scanResult != EOF && decodedValue == 0) {
      scanResult = scanf("%X", &input);
      if (scanResult - EOF) {
         decodedValue = LZWExpDecode(myExp, input);
      }
   }
   

   wasStopped = LZWExpStop(myExp);
   if (decodedValue == BAD_CODE) {
      printf("Bad code\n");
   }
   else if (wasStopped) {
      printf("Missing EOD\n");
   }
     
   LZWExpDestruct(myExp);
   free(myExp);
   if (report_space()) {
      printf("space: %d\n", report_space());
   }   
}
