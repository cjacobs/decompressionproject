#ifndef MYLIB_H
#define MYLIB_H

#define BITS_PER_BYTE 8
#define TRUE 1
#define FALSE 0
#define CHAR_SIZE 256
#define CODE_LENGTH 9
#define INT_SIZE 32
#define DICTDONE 4096

typedef unsigned char UChar;
typedef unsigned long ULong;
typedef unsigned int UInt;
typedef unsigned short UShort;

#endif

